package ts.connectn.game;

import java.util.ArrayList;
import java.util.List;
import ts.connectn.Chip;
import ts.connectn.ChipPosition;
import ts.connectn.GameResult;
import ts.connectn.GameResult.GameResultType;

public class GameImpl implements GameIfc {

    private final int rows;
    private final int columns;
    private final int chips;

    private List<List<Chip>> board;
    private GameResult gameResult;

    /**
     *
     * @param rows
     * @param columns
     * @param n
     */
    public GameImpl(int rows, int columns, int n) {
        this.rows = rows;
        this.columns = columns;
        this.chips = n;
        initializeBoard();
    }

    @Override
    public void putChip(Chip chip, int column) {
        addChipToColumn(chip, column);
        evalueMove(chip, column);
    }

    @Override
    public GameResult checkGameResult() {
        return gameResult;
    }

    private void initializeBoard() {
        board = new ArrayList<>();
        for (int i = 0; i < columns; i++) {
            board.add(new ArrayList<Chip>());
        }
    }

    private List<Chip> getColumn(int column) {
        return board.get(column);
    }

    private void addChipToColumn(Chip chip, int column) {
        getColumn(column).add(chip);
    }

    private int getActualRow(int column) {
        return getColumn(column).size() - 1;
    }
    
    private ChipPosition getLastChipPosition(int column) {
        return new ChipPosition(getActualRow(column), column);
    }

    private Chip getChipFromRowColumn(int row, int column) {
        Chip chip = null;
        List<Chip> rowsColumn = getColumn(column);
        if (!rowsColumn.isEmpty() && rowsColumn.size() > row) {
            chip = rowsColumn.get(row);
        }
        return chip;
    }

    private void evalueMove(Chip chip, int column) {
        evalueMoveVertical(chip, column);
        evalueMoveHorizontal(chip, column);
        evalueMoveDiagonal(chip, column);
    }

    private void setGameResult(List<ChipPosition> chipPositions, Chip chip, int column) {
        ChipPosition lastChipPosition = getLastChipPosition(column);
        if (chipPositions.size() == chips) {
            gameResult = new GameResult(GameResult.GameResultType.NOT_EVEN, chip, chipPositions.toArray(new ChipPosition[chipPositions.size()]), lastChipPosition);
        } else {
            Boolean columnFull = false;
            if(lastChipPosition.getRow() == (rows-1)) {
                columnFull = true;
            }
            gameResult = new GameResult(GameResult.GameResultType.EVEN, lastChipPosition, columnFull);
        }
    }

    private void evalueMoveVertical(Chip chip, int column) {
        List<ChipPosition> chipPositions = new ArrayList();
        List<Chip> rowsColumn = getColumn(column);
        if (rowsColumn.size() >= chips) {
            for (int i = rowsColumn.size() - 1; i >= 0; i--) {
                Chip chipColumn = rowsColumn.get(i);
                if (chipColumn.equals(chip)) {
                    chipPositions.add(new ChipPosition(i, column));
                } else {
                    break;
                }
            }
        }
        setGameResult(chipPositions, chip, column);
    }

    private void evalueMoveHorizontal(Chip chip, int column) {
        if (gameResult.getGameResultType().equals(GameResultType.EVEN)) {
            List<ChipPosition> chipPositions = new ArrayList();
            int actualRow = getActualRow(column);
            for (int i = 0; i < columns; i++) {
                List<Chip> rowsColumn = getColumn(i);
                if (rowsColumn.size() - 1 >= actualRow && rowsColumn.get(actualRow).equals(chip)) {
                    chipPositions.add(new ChipPosition(actualRow, i));
                } else if (chipPositions.size() == chips) {
                    break;
                } else {
                    chipPositions = new ArrayList();
                }
            }
            setGameResult(chipPositions, chip, column);
        }
    }

    private void evalueMoveDiagonal(Chip chip, int column) {
        evalueMoveDiagonalLeftUp(chip, column);
        evalueMoveDiagonalLeftDown(chip, column);
    }

    private boolean addChipPositionWhenRowColumnChipSame(List<ChipPosition> chipPositions, Chip chip, int row, int column) {
        boolean result = false;
        if (row >= 0 && row < rows && column >= 0 && column < columns) {
            Chip chipRowColumn = getChipFromRowColumn(row, column);
            if (chipRowColumn != null && chipRowColumn.equals(chip)) {
                chipPositions.add(new ChipPosition(row, column));
                result = true;
            }
        }
        return result;
    }

    private void moveChipPositionDiagonal(List<ChipPosition> chipPositions, Chip chip, int row, int rowSum, int column, int columnSum) {
        int positionRow = row;
        int positionColumn = column;
        while (addChipPositionWhenRowColumnChipSame(chipPositions, chip, positionRow, positionColumn)) {
            positionRow += rowSum;
            positionColumn += columnSum;
        }
    }

    private void evalueMoveDiagonalTrack(Chip chip, int column, int rowSum1, int columnSum1, int rowSum2, int columnSum2) {
        if (gameResult.getGameResultType().equals(GameResultType.EVEN)) {
            List<ChipPosition> chipPositions = new ArrayList();
            int actualRow = getActualRow(column);
            moveChipPositionDiagonal(chipPositions, chip, actualRow, rowSum1, column, columnSum1);
            moveChipPositionDiagonal(chipPositions, chip, actualRow + rowSum2, rowSum2, column + columnSum2, columnSum2);
            setGameResult(chipPositions, chip, column);
        }
    }

    private void evalueMoveDiagonalLeftUp(Chip chip, int column) {
        evalueMoveDiagonalTrack(chip, column, 1, 1, -1, -1);
    }

    private void evalueMoveDiagonalLeftDown(Chip chip, int column) {
        evalueMoveDiagonalTrack(chip, column, -1, 1, 1, -1);
    }

}
