package ts.connectn.controller;

import ts.connectn.Chip;
import ts.connectn.ChipPosition;
import ts.connectn.GameResult;

public class ResultMove {

    private final GameResult gameResult;

    public ResultMove(GameResult gameResult) {
        this.gameResult = gameResult;
    }

    public GameResult.GameResultType getGameResultType() {
        return gameResult.getGameResultType();
    }

    public Chip getWinnerChipType() {
        return gameResult.getWinnerChipType();
    }

    public ChipPosition[] getWinnerPositions() {
        return gameResult.getWinnerPositions();
    }

    public ChipPosition getLastChipPosition() {
        return gameResult.getLastChipPosition();
    }
    
    public Boolean getColumnFull() {
        return gameResult.getColumnFull();
    }
}
