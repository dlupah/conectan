package ts.connectn.controller;

import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import ts.connectn.Chip;
import ts.connectn.ChipPosition;
import ts.connectn.GameResult;
import ts.connectn.game.GameIfc;
import ts.connectn.game.GameImpl;

@Controller
@RequestMapping("/conectan")
@SessionAttributes("gameIfc")
public class ConectaNController {

    /*@ModelAttribute("gameIfc")
    public GameIfc addGameToSession() {
        GameIfc gameIfc = new GameImpl(6,7,4);
        return gameIfc;
    }*/
    
    @RequestMapping(value = "/createGame/{rows}/{columns}/{chips}", method = RequestMethod.GET)
    public @ResponseBody
    Boolean createGame(HttpSession session, Model model, @PathVariable int rows, @PathVariable int columns, @PathVariable int chips) {
        model.addAttribute("gameIfc", new GameImpl(rows, columns, chips));
        return true;
    }

    @RequestMapping(value = "/putChip/{chip}/{column}", method = RequestMethod.GET)
    public @ResponseBody
    ResultMove putChip(HttpSession session, @ModelAttribute("gameIfc") GameIfc gameIfc, @PathVariable Chip chip, @PathVariable int column) {
        
        gameIfc.putChip(chip, column);
        return new ResultMove(gameIfc.checkGameResult());
    }

}
